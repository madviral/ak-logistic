$(document).ready(function() {
  $("#menu").on("click", "a", function(event) {
    event.preventDefault();

    var id = $(this).attr("href"),
      top = $(id).offset().top;

    $("body,html").animate({ scrollTop: top }, 1000);
  });
});

$(document).ready(function() {
  $("#menu-second").on("click", "a", function(event) {
    event.preventDefault();

    var id = $(this).attr("href"),
      top = $(id).offset().top;

    $("body,html").animate({ scrollTop: top }, 1000);
  });
});

$(document).ready(function() {
  $("#title").on("click", "a", function(event) {
    event.preventDefault();

    var id = $(this).attr("href"),
      top = $(id).offset().top;

    $("body,html").animate({ scrollTop: top }, 1000);
  });
});

$(document).ready(function() {
  $('input[type="checkbox"]').click(function() {
    if ($(this).prop("checked") == true) {
      $(".first-label").hide();
      $(".second-label").show();
      $(".toggle-menu").show(400);
      $(".toggle-menu").addClass("menu");
      $(".toggle-menu").addClass("menu");
    } else if ($(this).prop("checked") == false) {
      $(".toggle-menu").hide(400);
      $(".first-label").show();
      $(".second-label").hide();
    }
  });
});
